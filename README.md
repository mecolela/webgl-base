# Modern WebGL Starter

## Installation

1. Run `yarn`. This will install all dependencies needed for the application to run.

2. Run `yarn dev`. This will watch and compile your Javascript files.

- Create a production build by running `yarn build`.

## Usage

This workflow is based on ES6 techniques and useful tools to make development of your THREE.js application as easy as possible!

#### Global variables
Variables in ES6 can be made easily accessible by using `imports`. Importing one of the objects below gives you the opportunity to change them in one place and use them in another (these will update automatically).

* **Props:** Used to store all THREE.js object: scene, renderer, camera and an object for structure elements like geometries and materials. These can be used in the GUI to update them in real-time.

* **Settings:** Used to store overall tool settings, camera variables, canvas background color, default device pixel ratio and a check for mobile devices.

* **Tools:** Used to store useful tools like a [GUI](https://github.com/dataarts/dat.gui) for changing variables in real-time, [orbit controls](https://github.com/mattdesl/three-orbit-controls) for user movement in the THREE.js world and a [stats monitor](https://github.com/mrdoob/stats.js/) to keep track of performance in the application. *These can all be turned on and off in the user settings.*

#### THREE.js
To create amazing things with THREE.js you should only have to focus on two things: Creating the magical environment and bringing it to life with movement.

* **Create:** After creating the basic requirements to run a THREE.js world the environment is created. This environment can be stored globally and adjusted by the GUI. This can be found in `environment.js`.

* **Render:** After the environment is created, and the optional tools are enabled, the render function is called and should optimally run at least 60 times a second. Besides the necessary things like updating the stats and setting a background color there's a clean function for updating the environment. This can be found in `render.js`;

#### Overall Workflow
To enhance the overall workflow these tools are really helpful to stay consistent with your code style and applying new techniques and standards while staying backwards compatible.

* **Webpack:** Used to compile Javascript for backwards compatibility. Running the development script watches and bundles Javascript as you develop and offers a localhost to see your application run on. Running the production script bundles the Javascript once and uglifies it to get the smallest size available.

* **ESLint:** Used to maintain Javascript code standards throughout the project. This workflow uses the [Airbnb Style Guide](https://github.com/airbnb/javascript) and can be adjusted to your liking in the `.eslintrc` file.

### TODO
1. Configure the webpack dev server to create a fluent workflow
