const CleanWebpackPlugin = require('clean-webpack-plugin')
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, '..', dir)
}

module.exports = {
  entry: resolve('/src/js/main.js'),
  output: {
    filename: 'script.js',
    path: resolve('dist')
  },
  resolve: {
    modules: [resolve('/node_modules/')],
    alias: {
      'dat.gui': 'dat.gui/build/dat.gui'
    }
  },
  plugins: [new CleanWebpackPlugin(['dist'], { root: resolve('') })],
  stats: {
    colors: true
  }
}
