import addLighting from './lighting'
import runStarter from './starter'

export default () => {
  addLighting()
  runStarter()
}
