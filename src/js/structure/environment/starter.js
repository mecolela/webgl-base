import * as THREE from 'three'

import props from '../config/props'

/**
 * Build the environment.
 */
export default () => {
  const geometry = new THREE.BoxGeometry(5, 5, 5)
  const material = new THREE.MeshPhongMaterial({
    color: 0xFFC107,
    emissive: 0x00BFA5,
    side: THREE.DoubleSide,
    shading: THREE.FlatShading,
  })

  props.structure.cube = new THREE.Mesh(geometry, material)
  props.scene.add(props.structure.cube)
}
