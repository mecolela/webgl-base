import props from '../config/props'

/**
 * Update the environment.
 */

export default () => {
  props.structure.cube.rotation.x += 0.01
  props.structure.cube.rotation.z += 0.01
}
