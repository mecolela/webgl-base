import settings from '../config/settings'

import addGUI from './gui'
import addOrbitControls from './orbit'
import addStats from './stats'

export default () => {
  if (settings.tools.gui) addGUI()
  if (settings.tools.orbit) addOrbitControls()
  if (settings.tools.stats) addStats()
}
